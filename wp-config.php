<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'metlaor.com');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'w36@b12');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'gZJK%gvVl_}NJ8^z0.sfynyC+>;I+2js-J]|#fb0j4VKXu A`#S3>. x`|a^J})V');
define('SECURE_AUTH_KEY',  'umbnHM?W<Fo9SAr4h-TL8ac-ZwPd.Yd$,%xg<;s>yOXoOB:.<2_g[[&{BAZyNW|D');
define('LOGGED_IN_KEY',    'M[Mg1&GHvu;VKD_c]@$;/rAf1rv<.z[qu/l7x!pvIxo6hAbt@OjakV_H~~<I2f,L');
define('NONCE_KEY',        'H(Pt1q:omss)ditB6-oBiv{oCDG3Ymn-l;Ja|]C-+NQy1&~{(`xhQ}QT{+ R6~CP');
define('AUTH_SALT',        'Z=n*;[U%!,09~,5k.`}].r1p~.LA4] M5Wf=0+UYq>WvW+[#`A-:NAP@A,u|,zFB');
define('SECURE_AUTH_SALT', 'UExTX)Q I4)}@6%ly@A8QhHbT<qVL.}lABIW,@&}mt)z3Q^eXz]g3i:<+8&3HM.|');
define('LOGGED_IN_SALT',   'w8<iZ+>y?TzsH:12aXCw-tu8?en}F+#!?~mI_H;q$;ASY<@Ni2zLS|D0O-9h`Q7=');
define('NONCE_SALT',       'C9^n7Hy/=QyviIZ=Q-kwkpG+,y{&h(0Fmw|Edb!a{=|IybaP~$@8IJ/,{$r~titJ');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'm3tl40rpr0j3ct_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
